#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time : 2022/1/10 下午 03:51
# @Author : Aries
# @Site : 
# @File : cnn.py.py
# @Software: PyCharm

import numpy as np
import cv2
import pandas as pd
from keras.utils import np_utils
from keras.models import Sequential, load_model
from keras.layers import Dense, Activation, Dropout, Dense, Dropout, Flatten, Conv2D, MaxPooling2D
import pymysql


def img_prcocess(url):
    img = cv2.imread(url)
    img = cv2.resize(img, (624, 416), interpolation=cv2.INTER_CUBIC)
    return img


def get_data(csv):
    img_arr = []
    files = pd.read_excel(csv)
    files_name = list(files['檔案名稱'])
    target = list(files['目標'])
    for name in files_name:
        img = img_prcocess('1204/' + name)
        img_arr.append(img)
    img_arr = np.array(img_arr)
    return img_arr, target


def cnn():
    img, label = get_data('cnnfiles.xlsx')
    img = img.reshape((img.shape[0], 624, 416, 3)).astype('float32')
    img /= 255
    label = np_utils.to_categorical(label)
    model = Sequential()
    # 建立卷積層1
    model.add(Conv2D(filters=32,
                     kernel_size=(3, 3),
                     padding='same',
                     input_shape=(624, 416, 3),
                     activation='relu'))
    # 建立池化層1
    model.add(MaxPooling2D(pool_size=(2, 2)))
    # 建立卷積層2
    model.add(Conv2D(filters=64,
                     kernel_size=(3, 3),
                     padding='same',
                     activation='relu'))
    # 建立池化層2
    model.add(MaxPooling2D(pool_size=(2, 2)))
    # 加入Dropout，避免overfitting
    model.add(Dropout(0.25))
    # 建立平坦層
    model.add(Flatten())
    # 建立隱藏層
    model.add(Dense(128, activation='relu'))
    # 建立輸出層
    model.add(Dense(3, activation='softmax'))
    # 打包
    model.summary()
    # 定義訓練方法
    model.compile(loss='categorical_crossentropy',
                  optimizer='adam', metrics=['accuracy'])
    # 訓練模型
    model.fit(x=img,
              y=label,
              validation_split=0.2,
              epochs=10, verbose=1)
    loss, accuracy = model.evaluate(img, label)
    model.save('cnn.h5')
    print("\nLoss: %.2f, Accuracy: %.2f%%" % (loss, accuracy * 100))




def save_db():
    # 連接資料庫
    conn = pymysql.connect(host='127.0.0.1', user='root', passwd='1234', database='volleyball', charset='utf8')
    cursor = conn.cursor()
    data = pd.read_excel('cnnfiles.xlsx')
    data = np.array(data)
    for i in range(0, len(data[:, 0])):
        cursor.execute(f"select tag from pictures where name = '{data[i, 0]}'")
        tag = cursor.fetchone()[0]
        if data[i, 1] == 0:
            tag += str(',合照')
        elif data[i, 1] == 1:
            tag += str(',個人照')
        else:
            tag += str(',其他')
        cursor.execute(f"update pictures set tag = '{tag}' where name = '{data[i, 0]}'")
    conn.commit()
    conn.close()

def model_test():
    model = load_model('cnn.h5')
    img = cv2.imread('1204/IMG_0016.jpg')
    img = cv2.resize(img, (624, 416), interpolation=cv2.INTER_CUBIC)
    img_arr = np.array([img])
    img = img_arr.reshape((img_arr.shape[0], 624, 416, 3)).astype('float32')
    img /= 255
    predict = model.predict(img)
    result = np.argmax(predict, axis=1)
    print(result[0])


if __name__ == '__main__':
    cnn()
    # model_test()
    # save_db()