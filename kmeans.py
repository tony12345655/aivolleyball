#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time : 2022/1/7 下午 10:29
# @Author : Aries
# @Site : 
# @File : main.py
# @Software: PyCharm


import numpy as np
import cv2
from sklearn.cluster import KMeans
import pandas as pd
import joblib
import pymysql


def img_flatten(url):
    img = cv2.imread(url)
    img = cv2.resize(img, (1920, 1080), interpolation=cv2.INTER_CUBIC)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(img, (5, 5), 0)
    canny = cv2.Canny(blurred, 30, 150)
    flatten_img = canny.flatten()
    return img, flatten_img


def get_data(csv):
    img_arr = []
    flatten_arr = []
    files = pd.read_excel(csv)
    files = list(files['檔案名稱'])
    for file in files:
        img, flatten_img = img_flatten('1204/' + file)
        img_arr.append(img)
        flatten_arr.append(flatten_img)
    img_arr = np.array(img_arr)
    flatten_arr = np.array(flatten_arr)
    return img_arr, flatten_arr, files


def kmeans():
    data, flatten_data, files = get_data('files.xlsx')
    km = KMeans(init="random", n_clusters=2)
    km.fit(flatten_data)

    final = []
    for i in range(2):
        index_arr = np.where(km.labels_ == i)[0]
        file_name = []
        for index in index_arr:
            file_name.append(files[index])
        final.append(file_name)
    df = pd.DataFrame(data=final)
    df.to_excel('result.xlsx', index=False)
    joblib.dump(km, 'KM.pkl')


def save_db():
    # 連接資料庫
    conn = pymysql.connect(host='127.0.0.1', user='root', passwd='1234', database='volleyball', charset='utf8')
    cursor = conn.cursor()
    data = pd.read_excel('result.xlsx')
    data = np.array(data)

    # 第一群
    for i in range(0, len(data[0])):
        if str(data[0, i]) != 'nan':
            cursor.execute(f"insert into pictures (name,tag) values('{data[0, i]}','比賽')")
    # 第二群
    for i in range(0, len(data[1])):
        if str(data[1, i]) != 'nan':
            cursor.execute(f"insert into pictures (name,tag) values('{data[1, i]}','休閒')")
    conn.commit()
    conn.close()


def model_test():
    model = joblib.load('KM.pkl')
    img = cv2.imread('1204/IMG_0532.jpg')
    img = cv2.resize(img, (1920, 1080), interpolation=cv2.INTER_CUBIC)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(img, (5, 5), 0)
    canny = cv2.Canny(blurred, 30, 150)
    flatten_img = canny.flatten()
    result = model.predict([flatten_img])
    print(result)


if __name__ == '__main__':
    # kmeans()
    save_db()
    # model_test()